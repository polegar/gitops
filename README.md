# k8s-gitops
---

Este repositório Git contém a definição completa de todas as aplicações Kubernetes em execução no cluster. Através do uso do [ArgoCD](), este repositório serve como a única fonte de verdade para o estado desejado das aplicações.

Mudanças nas aplicações são feitas através de commits neste repositório.Pull requests e revisões garantem que apenas mudanças seguras e confiáveis sejam aplicadas.

## Gitops

O GitOps é um modelo operacional para infraestrutura como código (IaC) que utiliza o Git como a fonte de verdade para definir e gerenciar o estado desejado de um sistema. Isso significa que todo o código necessário para configurar e gerenciar um sistema, desde a infraestrutura até as aplicações, é armazenado em um repositório Git. Ferramentas de automação, como o ArgoCD, monitoram esse repositório e garantem que o estado real do sistema sempre corresponda ao estado desejado definido no código.

## ArgoCD

O ArgoCD é uma ferramenta de código aberto para implantação contínua (CD) de aplicações em Kubernetes. Ele utiliza o GitOps como modelo operacional, o que significa que o estado desejado das aplicações é definido em um repositório Git e o ArgoCD trabalha para garantir que o estado real do cluster Kubernetes corresponda ao estado desejado.

O ArgoCD sincroniza automaticamente o estado do cluster com o estado desejado definido neste repositório.Isso garante que as aplicações estejam sempre em execução conforme o esperado, mesmo após falhas ou atualizações de infraestrutura.

Eventos e logs do ArgoCD fornecem visibilidade completa do processo de implantação e do estado das aplicações.
Isso facilita a identificação e resolução de problemas.
Benefícios do GitOps:

- Melhora na confiabilidade e na consistência das aplicações.
- Simplifica o gerenciamento de mudanças.
- Aumenta a colaboração entre equipes.
- Facilita a auditoria e o rastreamento de mudanças.
- Para mais informações sobre o GitOps e o ArgoCD:

