# Bootstrap
---

Neste diretório está contido todos os arquivos necessários para implantação do ambiente kubernetes. Cada subdiretório possui uma única e exclusiva função. A pasta [ArgoCD](./argocd/) possui todas as configurações iniciais do cluster que são configuradas logo após o cluster ser provisionado. A pasta [kubespray](./kubespray/) possui o projeto do [kubespray]() utilizado para fazer a implantação do cluster kubernetes utilizando ansible.

Dentro de cada diretório existe um tutorial explicando como o ambiente pode ser modificado.