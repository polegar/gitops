# Install Addons

> You must have:
> - Argo CLI installed. [Documentation](https://argo-cd.readthedocs.io/en/stable/cli_installation/)
> - kubectl and helm configured.

Add all repo to helm

```
argocd repo add https://prometheus-community.github.io/helm-charts --type helm --name prometheus-community
```

To get a admin password

```
kubectl get secret --namespace default prometheus-stack-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```


set default storageclass

```
kubectl patch storageclass cephfs -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```


Create secret to login in registry [Documentation](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)